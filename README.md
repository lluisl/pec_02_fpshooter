# Pec_02_FPShooter

Aventura en el Espacio

Objetivo del Juego
El objetivo del juego es ayudar al astronauta a escapar de los asteroides y encontrar una forma de volver a su nave principal. El jugador deberá superar diferentes desafíos en cada nivel para poder avanzar y completar la misión.

Historia
El astronauta estaba realizando una misión de exploración en el espacio cuando chocó contra un grupo de asteroides. Su nave quedó destrozada y dividida en partes por varios asteroides. Ahora, el astronauta debe encontrar una manera de salir de allí y volver a su nave principal.

Niveles
Nivel 1 - Asteroide 1: En el primer nivel, el jugador se encontrará en el asteroide 1. Aquí, el jugador aprenderá a moverse y a luchar contra robots asesinos que protegen una llave que le dará acceso a la puerta de una nave. El jugador debe encontrar la llave y escapar de este asteroide.
Nivel 2 - Nave: En el segundo nivel, el jugador entrará en la nave. El jugador debe encontrar una nueva llave para poder salir de esta nave. Primero, debe buscar un botón que quite el muro que bloquea una plataforma movible que lo llevará hacia arriba para acceder al lugar donde está la llave. Después de encontrar la llave, el jugador debe salir por la misma puerta por la que entró.
Nivel 3 - Asteroide 2 y 3: El tercer y último nivel empieza en el asteroide 2. El jugador debe encontrar la forma de llegar al tercer asteroide para abordar la última nave que lo llevará de vuelta a la nave principal. En este nivel, el jugador encontrará más enemigos y obstáculos para superar.

Mecánicas
Para el movimiento, el jugador tendrá la posibilidad de desplazarse a través del teclado usando las flechas de dirección para moverse hacia adelante, hacia atrás, a la izquierda y a la derecha. Esto le permitirá navegar por los diferentes niveles del juego y superar los obstáculos que se le presenten.
La cámara es una mecánica importante para que el jugador pueda tener una vista adecuada del escenario. Con el mouse, el jugador podrá rotar la cámara en todas las direcciones, permitiéndole tener una visión completa del entorno y de los enemigos que puedan aparecer. Además, la rotación de la cámara puede ser usada para ayudar al jugador a apuntar de manera más precisa y realizar disparos efectivos.
En cuanto a los disparos, el juego cuenta con dos armas diferentes: una pistola y una metralleta. El jugador podrá seleccionar la arma que prefiera y disparar usando el clic izquierdo para la pistola y el clic derecho para la metralleta. La precisión y la velocidad de disparo dependerán del arma que se esté utilizando, y es importante tener en cuenta que el jugador tendrá un número limitado de municiones para cada arma.
Estas mecánicas son cruciales para la jugabilidad del juego y para lograr que el jugador tenga una experiencia fluida y satisfactoria. La combinación de movimiento, cámara y disparos crea un entorno dinámico y emocionante que permitirá al jugador explorar los diferentes niveles, enfrentar desafíos y avanzar en la historia del juego.

Arte
Todos los assets, espacios, entornos, personajes fueron modelados por el autor con las herramientas de blender y unity probuilder.
Se utilizaron para las texturas las gratis de la pagina https://www.textures.com/library
EL HDRI para el environment global se descargo de la pagina https://polyhaven.com/

Código
SceneLoader
Este es un scriptque maneja la carga de escenas en el juego y controla la visibilidad de diferentes elementos del menú.
UIManager
Este código pertenece a un componente de interfaz de usuario. El componente se llama "UIManager" y contiene referencias a tres objetos de imagen: "gameOverImage", "gameWinImage" y "gamePauseImage".
FirstPersonController
Este código  maneja el movimiento y la interacción del jugador en una perspectiva de primera persona. También contiene metodos para reaccionar antes coalisiones con diferentes objetos en la escena.
HealtManager
Este código es un script para un objeto que maneja la salud y el escudo de un jugador en un juego. El script también maneja la lógica de recibir daño, curación y la muerte del jugador. 
GunController
Este código implementa un controlador de arma  que puede manejar dos tipos de armas: una pistola y una ametralladora. El controlador tiene un objeto de bala y un objeto creador de bala para cada tipo de arma.
Shot
Este es un script que se encarga de manejar el comportamiento de las balas cuando impactan en un enemigo. 
En términos de juego, esto significa que cuando un jugador dispara una bala y acierta en un enemigo, el enemigo es eliminado del juego y el jugador es recompensado con una caja de munición en la posición del enemigo. Esta caja de munición puede ser recolectada por el jugador para recargar su arma y seguir luchando contra otros enemigos.
IEnemyState
Este código se trata de la implementación de un sistema de estados para un enemigo en un videojuego. El enemigo tiene tres estados diferentes: Patrulla, Alerta y Ataque. Cada estado se implementa como una clase separada que implementa la interfaz "IEnemyState" y tiene sus propias funciones y comportamientos únicos.
EnemyAI
Este es un script que define el comportamiento de un enemigo en un juego.
La clase tiene varias variables públicas, incluyendo los estados de patrulla, alerta y ataque del enemigo (definidos como "PatrolState", "AlertState" y "AttackState"), la vida del enemigo, el tiempo entre disparos, la fuerza del daño, el tiempo de rotación, la altura del disparo, y los puntos de patrulla.
También hay algunas variables de juego de objetos como las balas que utiliza el enemigo y su creador de balas, así como la fuerza de la bala. Además, se definen un par de funciones para el movimiento del enemigo.
WinAndOver
Este código es utilizado para manejar la pantalla de victoria o derrota en un juego.
MovingPlatform
Este código es un script para un objeto que se mueve automáticamente en una plataforma móvil. 

