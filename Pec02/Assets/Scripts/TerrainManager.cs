using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    public Transform terrainTransform;

    private void Start()
    {
        terrainTransform.Rotate(new Vector3(0, 0, 180));
    }
}
