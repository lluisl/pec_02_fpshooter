using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    public GameObject ammoBoxCreate;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            
            Vector3 enemyPosition = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            
            if (gameObject.CompareTag("Bullet")) 
            {
                Debug.Log("Le he dado al enemigo");
                Destroy(gameObject);

               
                Instantiate(ammoBoxCreate, enemyPosition, Quaternion.identity);
            }
        }
    }
}

