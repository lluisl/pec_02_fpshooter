using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UIManager : MonoBehaviour
{
    //SCENE
    public GameObject gameOverImage;
    public GameObject gameWinImage;
    public GameObject gamePauseImage;


    // Start is called before the first frame update
    void Start()
    {
        gameOverImage.SetActive(false);
        gameWinImage.SetActive(false);
        gamePauseImage.SetActive(false);
    }
                  
    public void GameOver()
    {
        gameOverImage.SetActive(true);
    }

    public void GameWin()
    {
        gameWinImage.SetActive(true);
    }

}
