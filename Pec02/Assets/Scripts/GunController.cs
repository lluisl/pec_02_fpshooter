using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GunController : MonoBehaviour
{
    //VFX
    //public GameObject decalPrefab;
    //public AudioSource fireSound;

    //Gun
    public GameObject bulletGun;
    private GameObject clonBulletGun;
    public GameObject creatorBulletGun;
    public float forceGun;

    public int maxAmmoGun = 10; 
    public int currentAmmoGun; 

    public TextMeshProUGUI ammoTextGun; 

    //MachineGun
    public GameObject bulletGunMachine;
    private GameObject clonBulletGunMachine;
    public GameObject creatorBulletGunMachine;
    public float forceGunMachine;

    public int maxAmmoGunMachine = 25; 
    public int currentAmmoGunMachine; 

    public TextMeshProUGUI ammoTextGunMachine; 

   
    // Start is called before the first frame update
    void Start()
    {
        currentAmmoGun = maxAmmoGun;
        UpdateAmmoTextGun();
    }

    // Update is called once per frame
    void Update()
    {
        //Gun
        if (Input.GetMouseButtonDown(0) && currentAmmoGun > 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), out hit))
            {
                Debug.Log("He disparado");
                //fireSound.Play();
                
                clonBulletGun = GameObject.Instantiate(bulletGun, creatorBulletGun.gameObject.transform.position, Quaternion.Euler(0, 180, 0));
                clonBulletGun.GetComponent<Rigidbody>().velocity = creatorBulletGun.transform.forward * forceGun;
                Destroy(clonBulletGun, 0.1f);

                currentAmmoGun--; 
                UpdateAmmoTextGun(); 
            }
            
        }
        
        if (Input.GetKeyDown(KeyCode.R) && currentAmmoGun < maxAmmoGun)
        {
            int ammoToReload = maxAmmoGun - currentAmmoGun;
            currentAmmoGun += ammoToReload;
            Debug.Log("Ammo reloaded: " + ammoToReload);
            UpdateAmmoTextGun(); 
        }

        //MachineGun
        if (Input.GetMouseButtonDown(1) && currentAmmoGunMachine > 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), out hit))
            {
                Debug.Log("He disparado");
                //fireSound.Play();

                clonBulletGunMachine = GameObject.Instantiate(bulletGun, creatorBulletGunMachine.gameObject.transform.position, Quaternion.identity);
                clonBulletGunMachine.GetComponent<Rigidbody>().velocity = creatorBulletGunMachine.transform.forward * forceGunMachine;
                Destroy(clonBulletGunMachine, 0.1f);

                currentAmmoGunMachine--; 
                UpdateAmmoTextGunMachine(); 
            }

        }

        if (Input.GetKeyDown(KeyCode.R) && currentAmmoGunMachine < maxAmmoGunMachine)
        {
            int ammoToReload = maxAmmoGunMachine - currentAmmoGunMachine;
            currentAmmoGunMachine += ammoToReload;
            Debug.Log("Ammo reloaded: " + ammoToReload);
            UpdateAmmoTextGunMachine(); 
        }

    }

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("AmmoBox"))
        {
            currentAmmoGun += 10;
            Debug.Log("Ammo picked up. Current ammo: " + currentAmmoGun);
            Destroy(other.gameObject);
            
            UpdateAmmoTextGun(); 
        }
    }

    void UpdateAmmoTextGun()
    {
        ammoTextGun.text = currentAmmoGun.ToString();
    }

    void UpdateAmmoTextGunMachine()
    {
        ammoTextGunMachine.text = currentAmmoGunMachine.ToString();
    }

}
