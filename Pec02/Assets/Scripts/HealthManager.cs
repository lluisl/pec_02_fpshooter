using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public int maxHealth = 100;
    public int maxShield = 100;
    public int currentHealth;
    public int currentShield;

    public Slider healthSlider;
    public Slider shieldSlider;

    public bool isHitBy = false;
    public bool IsDead = false;

    private UIManager manager;

    public void Start()
    {
        currentHealth = maxHealth;
        currentShield = maxShield;
        manager = GetComponent<UIManager>();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
        healthSlider.value = currentHealth;
    }

    public void BreakShield(int damage)
    {
        currentShield -= damage;
        if (currentShield <= 0)
        {
            currentShield = 0;
           
        }
        shieldSlider.value = currentShield;
    }

    public void Heal(int amount)
    {
        currentHealth += amount;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        healthSlider.value = currentHealth;
    }

    public void Shield(int amount)
    {
        currentShield += amount;
        if (currentShield > maxShield)
        {
            currentShield = maxShield;
        }
        shieldSlider.value = currentShield;
    }

    // M�todo que se llama cuando el jugador es golpeado por un enemigo
    public void OnHitByEnemy()
    {
        if (currentShield > 0 || !isHitBy) 
        {
            if (currentShield > 0) 
            {
                BreakShield(50);
            }
            else if (currentHealth > 0) 
            {
                TakeDamage(10); 
            }

            isHitBy = true;
            Invoke("ResetIsHitBy", 2.0f);
        }
    }

    // M�todo que se llama cuando el jugador toma un item de salud
    public void OnPickUpHealthItem()
    {
        Heal(50);
        GameObject healItem = GameObject.FindWithTag("Heal"); 
        if (healItem != null)
        {
            Destroy(healItem); 
        }
    }

    public void OnPickUpShieldItem()
    {
        Shield(50);
        GameObject shieldItem = GameObject.FindWithTag("Shield"); 
        if (shieldItem != null)
        {
            Destroy(shieldItem); 
        }
    }

    // M�todo que se llama cuando el enemigo es golpeado por el jugador
    public void OnHitByPlayer()
    {
        TakeDamage(50);
    }

    

    public void Die()
    {
        
        manager.GameOver();
        IsDead = true;
        

    }

    public void ResetIsHitBy()
    {
        isHitBy = false;
    }
}

