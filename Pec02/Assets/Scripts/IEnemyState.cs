using UnityEngine;

public interface IEnemyState
{
    
    void UpdateState();
    void GoToAttackState();
    void GoToAlertState();
    void GoToPatrolState();
    

    void OnTriggerEnter(Collider col);
    void OnTriggerStay(Collider col);
    void OnTriggerExit(Collider col);

    void Impact();
}




public class PatrolState : IEnemyState
{
    enemyAI myEnemy;
    private int nextWayPoint = 0;

    public PatrolState(enemyAI enemy)
    {

        myEnemy = enemy;
    }

    public void UpdateState()
    {
        myEnemy.myLight.color = Color.green;

        myEnemy.navMeshAgent.destination = myEnemy.wayPoints[nextWayPoint].position;

        if (myEnemy.navMeshAgent.remainingDistance <= myEnemy.navMeshAgent.stoppingDistance)
        {
            nextWayPoint=(nextWayPoint + 1) % myEnemy.wayPoints.Length;
        }
    }

    public void Impact()
    {
        GoToAlertState();
    }

    public void GoToAlertState()
    {
        myEnemy.Stop();
        myEnemy.currentState = myEnemy.alertState;
    }

    public void GoToAttackState() 
    {
        myEnemy.Stop();
        myEnemy.currentState = myEnemy.attackState;
    }

    public void GoToPatrolState() { }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
            GoToAlertState();
    }
    public void OnTriggerStay(Collider col) 
    {
        if (col.gameObject.tag == "Player")
            GoToAlertState();
    }
    public void OnTriggerExit(Collider col) { }
}

public class AlertState: IEnemyState
{
    enemyAI myEnemy;
    float currentRotationTime = 0;

    public AlertState(enemyAI enemy)
    {
        myEnemy = enemy;
    }

    public void UpdateState()
    {
        myEnemy.myLight.color= Color.yellow;

        myEnemy.transform.rotation *= Quaternion.Euler(0f, Time.deltaTime * 360 * 1.0f / myEnemy.rotationTime, 0f);
        Debug.Log("Estan en alerta");

        if (currentRotationTime > myEnemy.rotationTime)
        {
            currentRotationTime = 0;
            GoToPatrolState();
            Debug.Log("Sigo caminando");
        }
        else
        {
            RaycastHit hit;

            if (Physics.Raycast(new Ray(new Vector3(myEnemy.transform.position.x, myEnemy.transform.position.y, myEnemy.transform.position.z), myEnemy.transform.forward * 100f), out hit))
            {
                                
                if (hit.collider.gameObject.tag == "Player")
                {
                    Debug.Log("Te estas atacando");
                    Debug.Log(hit.collider.name);
                    GoToAttackState();
                }
            }
        }
        currentRotationTime += Time.deltaTime;
    }

    public void Impact()
    {
        Debug.Log("Le hemos disparados");
        GoToAttackState();
    }

    public void GoToAlertState() { }

    public void GoToAttackState()
    {
        myEnemy.currentState = myEnemy.attackState;
    }

    public void GoToPatrolState()
    {
        myEnemy.Resume();
        myEnemy.currentState = myEnemy.patrolState;
    }

    public void OnTriggerEnter(Collider col) { }
    public void OnTriggerStay(Collider col) { }
    public void OnTriggerExit(Collider col) 
    {
        if (col.gameObject.tag == "Player")
        {
            GoToPatrolState();
            
        }
    }
   
}

public class AttackState : IEnemyState
{
    enemyAI myEnemy;
    float actualTimeBetweenShoots = 0;
        
    //private bool playerDetected = false;
    public AttackState(enemyAI enemy)
    {
        myEnemy = enemy;
    }

    public void UpdateState()
    {
        
        myEnemy.myLight.color = Color.red;
        actualTimeBetweenShoots += Time.deltaTime;
    }

    public void Impact() { }

    public void GoToAttackState() { }
    public void GoToPatrolState() 
    {
        myEnemy.Resume();
        myEnemy.currentState = myEnemy.patrolState;
    }

    public void GoToAlertState()
    {
        myEnemy.currentState = myEnemy.alertState;
    }

    public void OnTriggerEnter(Collider col) { }
    
    public void OnTriggerStay(Collider col)
    {
        Vector3 lookDirection = col.transform.position - myEnemy.transform.position;

        myEnemy.transform.rotation = Quaternion.FromToRotation(Vector3.forward, new Vector3(lookDirection.x, 0, lookDirection.z));
        
        
            if (actualTimeBetweenShoots > myEnemy.timeBetweenShoots)
            {
                actualTimeBetweenShoots = 0;
                myEnemy.BulletEnemy();
                
                Debug.Log("Esta disparando");
                
            }
            
        
        
            
    }
    public void OnTriggerExit(Collider col) 
    { 
        if (col.gameObject.tag == "Player")
        {
            GoToPatrolState();
            

        }

    }


}
